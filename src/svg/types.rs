use serde::{Deserialize, Serialize};

pub trait Shape {
    fn area(&self) -> f32;
    fn position(&self) -> (f32, f32);
    fn stroke(&self) -> &str;
    fn fill(&self) -> &str;
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Rect<'a> {
    fill: &'a str,
    stroke: &'a str,
    width: f32,
    height: f32,
    x: f32,
    y: f32,
}

impl<'a> Rect<'a> {
    pub fn new(fill: &'a str, stroke: &'a str, width: f32, height: f32, x: f32, y: f32) -> Self {
        Self {
            fill,
            stroke,
            width,
            height,
            x,
            y,
        }
    }
}

impl<'a> Shape for Rect<'a> {
    fn area(&self) -> f32 {
        self.width * self.height
    }

    fn position(&self) -> (f32, f32) {
        (self.x, self.y)
    }

    fn stroke(&self) -> &str {
        &self.stroke
    }

    fn fill(&self) -> &str {
        &self.fill
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Canvas {
    pub width: u32,
    pub height: u32,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Sprite<T: Shape> {
    pub canvas: Canvas,
    pub shapes: Vec<T>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Assets<T: Shape> {
    pub assets: Vec<Sprite<T>>,
}
