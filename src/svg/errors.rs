use thiserror::Error;

#[derive(Debug, Error)]
pub(crate) enum IOError {
    #[error("I/O process failed.")]
    FileIO(#[from] std::io::Error),

    #[error("Parsing JSON failed.")]
    ParseError(#[from] serde_json::Error),
}
