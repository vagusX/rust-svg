use crate::arg_parse::Opt;
use std::{
    fs::File,
    io::{prelude::*, BufReader, BufWriter},
};

mod errors;
mod types;
use errors::IOError;
use types::{Assets, Rect};

fn get_file_handles(
    input_path: &str,
    output_path: &str,
) -> Result<(BufReader<File>, BufWriter<File>), IOError> {
    let input_file_handle = BufReader::new(File::open(input_path)?);
    let output_file_handle = BufWriter::new(File::create(output_path)?);

    Ok((input_file_handle, output_file_handle))
}

fn parse(input_path: &str, output_path: &str) -> Result<(), IOError> {
    let (mut rx, mut tx) = get_file_handles(input_path, output_path)?;

    let header = "<?xml version=\"1.0\" standalone=\"no\"?>".as_bytes();
    tx.write_all(header)?;

    let mut buffer = String::new();
    rx.read_to_string(&mut buffer)?;

    let processed: Assets<Rect> = serde_json::from_str(&buffer)?;

    println!("{:?}", processed);

    Ok(())
}

pub(crate) fn init(args: Opt) -> Result<(), IOError> {
    parse(&args.input, &args.output)?;

    Ok(())
}
