use thiserror::Error;

#[derive(Debug, Error)]
pub(crate) enum LoggingError {
    #[error("Environment variables could not be loaded.")]
    DotEnv(#[from] dotenv::Error),

    #[error("Logging interface failed.")]
    Interface(#[from] log::SetLoggerError),
}
