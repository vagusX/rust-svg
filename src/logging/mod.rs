mod errors;
use errors::LoggingError;

#[allow(unused_macros)]
macro_rules! try_log {
    ($expr:expr) => {
        match $expr {
            ::std::result::Result::Ok(val) => val,
            ::std::result::Result::Err(err) => {
                log::error!("{}, err");
                return ::std::result::Result::Err(::std::convert::From::from(err));
            }
        }
    };
    ($expr:expr,) => {
        $crate::logging::try_log!($expr)
    };
}

pub(crate) fn init() -> Result<(), LoggingError> {
    dotenv::dotenv()?;

    env_logger::init();

    Ok(())
}
