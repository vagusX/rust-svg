use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "svg")]
pub(crate) struct Opt {
    /// Input json file, used for generating shapes and graphics
    #[structopt(short, long)]
    pub(crate) input: String,

    /// Output file
    #[structopt(short, long)]
    pub(crate) output: String,
}

pub(crate) fn parse() -> Opt {
    Opt::from_args()
}
