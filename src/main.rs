use anyhow::Result;

#[macro_use]
mod logging;
mod arg_parse;
mod svg;

fn main() -> Result<()> {
    logging::init()?;

    let args = arg_parse::parse();

    svg::init(args)?;

    Ok(())
}
